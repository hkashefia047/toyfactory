package com.company;

public class Car extends Toy {
    public Car(double basePrice, ToySize size) {
        super(basePrice, size);
    }
    public double getPrice() {
        if (size==ToySize.SMALL) {
            return BasePrice;
        }else if (size==ToySize.MEDIUM){
            return BasePrice*2;
        }else {
            return BasePrice*3;
        }
    }
}
