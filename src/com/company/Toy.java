package com.company;

public class Toy {
    double BasePrice;
    ToySize size;
    double Price;



    public Toy(double basePrice, ToySize size) {
        BasePrice = basePrice;
        this.size = size;
    }

    public double getPrice() {
        if (size==ToySize.SMALL) {
            return BasePrice;
        }else if (size==ToySize.MEDIUM){
            return BasePrice*1.5;
        }else {
            return BasePrice*2;
        }
    }

    public double getBasePrice() {
        return BasePrice;
    }

    public void setBasePrice(double basePrice) {
        BasePrice = basePrice;
    }

    public ToySize getSize() {
        return size;
    }

    public void setSize(ToySize size) {
        this.size = size;
    }
}
